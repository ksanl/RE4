## [2025倾龙X小狂龙 官方地址](https://gitee.com/lioncky/RE4#2025倾龙x小狂龙-官方地址) ![](https://gitee.com/dromara/go-view/badge/star.svg?theme=dark) ![](https://gitee.com/dromara/go-view/badge/fork.svg?theme=white)  *[👑一键安装教程](#7下载)*
> ⚠️ **注意：本项目是非开源项目，本仓库仅用来管理需求和用户反馈**  *[弹错报错解决方案](#8)*  

![](https://gitee.com/lioncky/RE4/raw/master/imgs/vagtMT.png)
### re4tweaks汉化版与原版差异一览
差异| re4tweaks | *2025小狂龙(KL.dll)*| [2023威虎](https://gitee.com/lioncky/RE4/blob/master/README_YH.md)
---|---|---|---
源码| 1.9.1|1.9.1+倾龙+改版闪退修复|1.5.0+威虎部分
特性|标准| **标准+汉化+极致的优化**|标准+汉化
大小|屎山级>10MB|极小<2MB|大>5MB
速度|3.5秒|👏1.6秒|2.8秒
维护|❌|✅|❌
汉化|❌|✅|✅
自动更新|✔️|✅|✅
鼠标转向|✅|✅|✅
[改名加载](#9)|✅|✔️|✅
价格|✅**0$**|✅**0￥**|✅0￥

	
### 改版列表与支持情况
>  ✅完美支持  | ✔️没大问题 | ❌无法加载 | - 未测试<br>

改版| re4tweaks | *2025小狂龙*| 2023威虎
---|---|---|---
万人斩(ArrangeMod)|✅|✅|✅
无限敌人(Unlimit)|✅|✅|-
强者为王(PowerOfEnemy)|✅|✅|-
死期已至(TimeToDie)|✔️|✅|-
死期将至(PrepareToDie)|✔️|✅|-
邪恶上升3.2(RisingOfEvil)|✔️|✅|✔️
逃到死(EscapeUntilDie)|✔️|✅|✔️
邪恶监狱(PrisonOfEvil)|✔️|✔️|✔️
邪恶后转(RisingOfEvil)|-|-|✔️
发狂释放1.6(修复2)|-|-|✔️
混沌世界2023|-|-|✔️
死神2.175|❌|✅|-
亡命天涯|❌|-|-
围师必阙2.0(修复4)|❌|-|-
暴走V4(补丁Patch)|✅|✅|✅

> 👏👏👏 感谢各位高端玩家的测试 和 各路神仙宝子的赞助 本项目已经6年没打烊啦~<br>
> 👏👏👏 感谢各位高端玩家的测试 和 各路神仙宝子的赞助 本项目已经6年没打烊啦~<br>
> 👏👏👏 感谢各位高端玩家的测试 和 各路神仙宝子的赞助 本项目已经6年没打烊啦~

![](https://gitee.com/lioncky/RE4/raw/master/imgs/2025-02-11/AGBA3R9dQZRvgxCX.png)
### 
```
1.汉化版作者为原版作者之一 二者基于同一份源码本质上没有差异
2.出于一些兼容原因 原版已不再更新 且加载过慢 汉化版进行了提速处理
3.汉化版新增新存档解锁专家模式和所有内容等新内容 而无需手动制作 即装即玩
4.原版存在诸多问题 例如游玩邪恶上升湖内商人会导致严重卡帧闪退 汉化版修复了这些问题
5.自倾龙X起不再支持报毒的dinput8 X3DAudio等改名dll的加载方式 由狂龙驱动代替加载功能
```
### 2025.1.19小狂龙特性
``` 
1.基于XEngine 该引擎为2025跨时代引擎 后续均将基于此进行开发 不再改动
2.解决存档不互通的问题 小狂龙允许加载任何人的存档而不会导致坏档 也不会影响原存档的主人
3.解决改版接口问题 写在倾龙内则略显混乱 倾龙用来开G和测试 狂龙用来写改版 不会乱
4.且QL基于re4tweaks的屎山代码过于笨重 本人对所有作品的有强品质控
5.狂龙可以驱动倾龙 二者于v4.0正式版完美共存 且支持英文原版
```

### 2025小狂龙简介
时间| 最新更新 |lang|作用|实现|改名加载
---|---|---|---|---|---
2025.1| steam_api |C|小狂龙引擎|XEngine| ✔️ 
-|[+] KM(imalloc)|C|倾龙超程驱动|Microsoft微软官方团队| -
-|[+] KO(fficial)|C|半条命官方驱动|Steam官方团队(steam_api.dll改名)| -
-|[+] KS(cript)|C++|限定版脚本驱动 | WinNs(脚本限定 基于KL而非英文版) | -
-|[+] KL(ong)|C++|倾龙QLX修改驱动|WinNs(re4tweaks中文XEngine限定版)| ✔️ 
2024.6|倾龙QL|C++|参考倾龙| WinNs(re4tweaks_CN) | ✔️
2023.3|陨兔|C++|参考RE4R陨兔|WinNs|-
2022.5|威虎|C++|参考陨虎| Wincpp(旧re4tweaks_CN) | ✅ 
2021|-|C++|-| Wincpp|-|
2020.5|邪恶传说|C++|参考3DM| KsaEngine(这时候re4tweaks还没出生呢) | ❌

> 改名加载 ✅:表示支持[列表全部](#9)
> ✔️:表示支持dinput8和X3DAudio1_7.dll <br>
> 
> 

#### 0.它是什么
```
1.一个学习补丁
2.倾龙X等内容的驱动装置
3.支持需要正版才能游玩的改版
4.游戏改版装置 可以用它自定义添加敌人和事件
5.高性能模组驱动器 可以驱动QL超程 改善游戏卡顿
```
#### 1.如何存档互通
``` 
答：狂龙的存档位置为Bin32和BIO4同级目录 即在这俩文件夹旁边即可看到 savegame00.sav
你可以把存档直接丢在这里即可加载 也可以游戏内提示创建后在这里找到
在同级目录的存档会强制加载并忽略一些导致无法正常加载存档的规则
```
#### 2.如何加载人物MOD
``` 
答：狂龙的MOD位置为Bin32和BIO4同级目录GSBZ 即在这俩文件夹旁边
你需要将MOD的BIO4文件夹内的内容全部解压到GSBZ内 没有这个文件夹就创建一个
那么进游戏即可正常加载 包括bgm也可以通过这种方式进行替换
```
#### 3.KM倾龙超程是什么
```
答：倾龙超程为高端cpu设计打造 解决游戏内存访问过于缓慢的问题 
该问题会导致游玩万人斩等敌人数量过多的改版时的卡顿
如不需要请删除 KM.dll
```
#### 4.KO如何使用正版steam_api.dll/我的存档丢失了?
```
答：由于2025狂龙使用了正版steam_api.dll的名称
因此如果出现存档丢失的问题 其实并不是存档真的丢了 而是原本是正版 现在变成学习版了
如使用steam原版存档安装KO.dll即可(就是原本steam_api.dll改了个名字)
```
#### 5.如何与re4tweaks一起使用
```
答：如果是汉化版则使用v4倾龙即可 如不需要请删除 KL.dll
汉化版：安装KL.dll

英文原版：请使用dinput8或X3DAudio1_7等dll名称直接进行加载 
需要英文版配置关闭Misc->SkipIntroLogos和FrameRate->EnableFastMath两个选项

可能会出现不兼容的情况 部分改版使用英文版的文件直接改为中文版即可
```
#### 6.如何与游侠4.5汉化版一起使用
```
答：将游侠4.5的steam_api.dll改为K7.dll和即可使用
或者将小狂龙引擎KL.dll改为dinput8.dll或X3DAudio1_7.dll 
```
#### [7.下载](https://gitee.com/lioncky/RE4/releases)
最新完整版压缩包 https://gitee.com/lioncky/RE4/releases
![](/imgs/2025-02-11/xOhrQxyDQtJ33oU5.png)
> 下载完成后放置到游戏Bin32路径下即可~
> 
> 如果是steam正版 先将steam_api.dll改名为KO.dll 再安装即可
> 
如果您遇到了困难且尝试后任无法解决 请前往 [群聊]([https://jq.qq.com/?_wv=1027&k=2uH0Jcg7](https://jq.qq.com/?_wv=1027&k=2uH0Jcg7)) 24h在线获取帮助 

管理员ID：局外 仅需88R远程省心安装套餐 计赞助和感谢名单

#### 8
#### 8.我无法正常启动游戏
##### DNS
> DNS异常表示服务器已变更 需要将倾龙更新到最新版本<br>
> 点击[此处](https://gitee.com/lioncky/RE4/releases)找最新版下载 覆盖到Bin32即可 后续会自动更新
> 

##### Patterns/路径异常
![](/imgs/2025-02-10/LYZb8qhjIDcWzXZn.png)
> 查看Bin32路径下的dll文件信息是不是re4tweaks 1.9.1英文版删除即可<br>
> 汉化版和英文版一起装就会出现一个找不到补丁地址的情况也就是Patten异常<br>
> 这里第二行已经提示你 WINMM.dll 是英文版 删除他就可以了
> 
##### winCRT运行库异常
![](/imgs/2025-02-09/pq1lEIVVVw3c35PN.png)
> 1.官方下载微软运行库即可 [官方下载地址](https://learn.microsoft.com/zh-cn/cpp/windows/latest-supported-vc-redist?view=msvc-170) 这个地址一直在变<br>
> 2.直接下载这个 [https://aka.ms/vs/17/release/vc_redist.x86.exe](https://aka.ms/vs/17/release/vc_redist.x86.exe)
> ![输入图片说明](/imgs/2025-02-11/XJeN9y3rW4rJnjHX.png)
> 3.群内搜索 VC_redist.x86 直接下载安装 
> 

#### 9
#### 9.被游戏所调用的DLL列表
```
    d3d9.dll
    ole32.dll
    GDI32.dll
    WINMM.dll
    DINPUT8.dll
    SHELL32.dll
    SHLWAPI.dll
    d3dx9_43.dll
    ADVAPI32.dll
    OLEAUT32.dll
    XINPUT1_3.dll
    X3DAudio1_7.dll
```
> ✔️其中 DINPUT8 和 X3DAudio1_7 时至今日任收支持<br>
> ✅其他仅2022 2.X的陨虎以及英文版re4tweaks全部支持
> 
#### 

### 初代引擎版本 此时还没有re4tweaks哦~
> 2020国内就有eoe了 妥妥的原创国人神作 ❌✔️✅👑🏅🗝️💰[⭐](https://emojipedia.org/star)
> 
> *如果需要查看倾龙相关文档 请 [传送至2024倾龙](https://gitee.com/lioncky/RE4/blob/master/README_QL.md)*
> 
> *如果需要查看倾龙前置 请 [传送2023陨虎](https://gitee.com/lioncky/RE4/blob/master/README_YH.md)*
![](https://att.3dmgame.com/att/forum/202202/28/215441zzvr8b8tn5x01580.png)

## 生4重制版RE4REMAE请参考[陨兔](https://gitee.com/lioncky/RE4/blob/master/README_R.md)
![](https://att.3dmgame.com/att/forum/202305/20/054604xbelounwlnywn5lf.jpg)

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwMzU2MDE3NDQsMzQ0NDcxOTE3LDg5MD
IzMDgxMCwtMjA0MTk0MzAzNCwtMTgwODE4Njk2Niw1NjQ0ODIz
ODQsMTM3ODgwNjAyMCwtMjA4Nzk4MDMzNiwtMTU0Mzc2MDc2MC
wyMjg3MTc2NTksMTU4MjI4Njc4LC05OTQyMTc0NjAsMTAyOTUy
MTYyLDk4MDg4NzU1MCwtMTE1MTc1NDgwMiwtNzg0OTY1NDI5LC
0xNzU0ODI4MDEwLDIwMDQ3MTI4NTUsLTIwMzY4ODU5NTEsLTU0
ODE3MzM4MF19
-->