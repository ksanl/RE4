## GS引擎：倾龙改版整合页 (mod.re4gs.cn [#](https://gitee.com/lioncky/RE4/blob/master/Bio4/MOD.md))

0.查看[刷怪模块指令](/Bio4/DANMU.md)<br>

由 3DM盖世班主 提供的 <br>
1.**[「生4UHD倾龙整合」百度网盘](https://pan.baidu.com/s/13EwkRCCfLUfqO54SBvpEng?pwd=2236)** (推荐)<br>
2.**[「生4UHD倾龙整合」阿里云盘 ](https://www.alipan.com/s/i2zBVW4aXqR)** 密码: 2236 (不再支持)<br>
阿里云盘不限速下载请使用 [小白羊学习版阿里云盘](https://gsbz.lanzoux.com/iAEgD1t8viyh) <br>
> 有想一起整合在这里的改版 请自行反馈<br>
> 部分改版名字存在暴力内容无法正常展示 <br>
> 推荐认准英文名称 请大家理解下<br>
> [自行兼容改版教程](#如何自行兼容改版并提交)
> 

注意！！！<br>
下面的地址为作者发布地址或个人页 非专业玩家使用上面的整合即可 <br>
下面的地址为作者发布地址或个人页 非专业玩家使用上面的整合即可 <br>
下面的地址为作者发布地址或个人页 非专业玩家使用上面的整合即可 <br>

#### MOD10001

> ***2024折戟沉沙  (Torment and Death)***
> 
> - [x] 玩家整合
> - [x] 支持解压直玩
> - [x] 引擎整合于 v3910 
> - [x] 该改版支持GSBZ式加载
> 
>  原作者版权地址 [源Youtube](https://www.youtube.com/watch?v=38wmVb_z0JQ)  [源下载地址](https://www.mediafire.com/file/uh8rjnih960nruv/Torment+and+death.rar/file)
>  
  
![](/imgs/2024-04-02/ZPHU0bqeQkN7WTEb.png)

#### MOD10002
> ***强者为王  ( The Power Of Enemy)*** 
> - [x] 推荐指数：🕹️🕹️🕹️🕹️🕹️
> - [x] 稳定指数：🏅🏅🏅🏅🏅
> <br>
> - [x] 玩家整合
> - [x] 支持解压直玩
> - [x] 该改版支持GSBZ式加载
> - [x] 引擎整合于 v3910 3916 3920完美包已上传 重新下载即可
> - [x] **测试连打40分钟到通关未出现任何的闪退情况**🏅
>  
> 出处： [源Youtube](https://www.youtube.com/watch?v=FzokTrA7C4k&t=0s)  [源下载地址](https://www.mediafire.com/file/tl9gsr1fjq0cwwh/The+Power+Of+Enemy+Full+Release.rar/file) [源补丁地址](https://www.mediafire.com/file/egjfaysvv3vuuhr/Fix+Bug+COPY+PASTE+REPLACE.rar/file) [密码 MODINDONESIA]
> 
![](/imgs/2024-04-02/vNWPaL60r4QgvaX2.png)

#### MOD10006
> ***终极世界( World Super)*** 
> 版本特性: 钥匙需靠近门手动在背包内使用
> - [x] Raz0r628K
> - [x] 引擎**兼容**于 v3916
> - [x] 20G自带游戏+高清修正+盖世学习补丁 解压直玩
> - [ ] 该改版暂不支持GSBZ式加载MOD
> 
> [作者MOD站发布页地址](https://residentevilmodding.boards.net/thread/15153/resident-evil-world-download-alpha)<br>
> 注：内含大量烧脑解密 场景丰富 强度高 但稳定性差 多存档
 
![输入图片说明](/imgs/2024-03-28/2By8Twve3kInb4g0.png)

#### MOD10005
> ***邪恶监狱 ( Prison of Evil)*** 
> 版本特性: 特定怪阵亡才可进入下个场景 蓝牌在水槽里
> 
> - [x] 解压粘贴
> - [x] 引擎**兼容**于 v3912
> - [x] 该改版支持GSBZ式加载
> - [x] 无需28G高清+内置盖世学习补丁
> 
> [作者Youtube](https://www.youtube.com/@LeoLimaEditor)<br>
> 注：农场不要跳关 自己找找怎么前往山顶<br>
> 已发现的闪退点1：别把雷丢进农场怀表水槽 炸蓝牌丢外面<br>
 ![输入图片说明](/imgs/2024-03-28/mjV80nEHpSoKHDCy.png)


#### MOD10004
> ***地狱之路  ( Road To Hell)*** 
> - [x] 稳定指数：🏅🏅🏅
> 
> - [x] 解压直装
> - [x] 引擎**兼容**于 v3910  
> - [x] 该改版支持GSBZ式加载
> - [+] Compatible with tweaks=1
> 
> 出处： [Blibli](https://space.bilibili.com/471442891) 
> 

#### MOD10003
> ***极端恐惧  ( JUMP SCARE)*** 
> - [x] 稳定指数：🏅🏅🏅🏅🏅
> 
> - [x] 国内大神
> - [x] 支持解压直玩
> - [x] 引擎**兼容**于 v3910 
> - [x] 该改版支持GSBZ式加载
> - [+] Compatible with tweaks=1
> - [+] Can spawn more kinds of enemise=0
> 
> 出处：[Blibli](https://www.bilibili.com/video/BV12v4y1j7ax) 
> 


#### MOD10000
> ***邪恶上升3.0  ( Rasing of Evil ) 参考下面自己试试整合吧*** 
> - [x] 该改版支持GSBZ式加载
> - [x] 仅支持v1.0.6版本游玩
> - [x] 支持游侠4.5汉化
![](/imgs/2024-03-21/mrVTJqactaOftFGH.png)
<br><br>

## 如何自行兼容改版并提交
1.修改改版的BIO4文件夹名字为GSBZ [视频解说](https://www.bilibili.com/video/BV1cJ4m177PB)<br>
2.检查Bin32是否存在其他dll 如果存在请检查同名ini或者txt

> 存在 Compatible with tweaks=0 修改为1<br>
> 存在 Can spawn more kinds of enemise=1 改为 0<br>
> 
> 想加武器和其他背包MOD 直接复制到GSBZ文件夹里面去就可以了<br>
> 不想玩了 删除GSBZ文件夹即可 或改名方便下次游玩

3. 提交
> 点击issue问题 -> 新建issue -> 需求写希望整合此改版并提交百度网盘链接给我即可
> 
![](/imgs/2024-03-23/kLfewAiLzVW2bioR.png)

# Table

Name &nbsp; &nbsp; &nbsp; &nbsp; | Multiline &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br>header  | [Link&nbsp;](#link1)
:------|:-------------------|:--
Value-One | Long <br>explanation <br>with \<br\>\'s|1
~~Value-Two~~ | __text auto wrapped__\: long explanation here |25 37 43 56 78 90
**etc** | [~~Some **link**~~](https://github.com/mekhontsev/imgui_md)|3
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzODgyNDQ0MDUsMjEyNzc2MTI3NCwtMT
c2ODczODQ0NSwxNTE2MzA4OTgwLDE3MzIzMzk1MzEsNjU2NDY0
ODk1LC0xNDc0MDI5MzE2LC0yMTMzNDU0MTE0LC03NjI3MzUxNj
EsLTIxNDQ5MjA2ODAsNTY1OTIxMjI2LC0zNTM3MDU5ODAsLTEx
Mzg0MDcyNjgsLTExNzIyODA0NzksMTA4NjQ1MjM0MCwtNTAwND
g5MzYwLDM1MDE4ODE1NiwyMDgzMTI0Njg1LC0xMjg3NTEyNzgs
LTQ4NzcyMTgyMl19
-->