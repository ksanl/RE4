# 生化危机4终极高清版UHD列表整合
> 由 [@3DM盖世班主](https://bbs.3dmgame.com/thread-6423177-1-3.html) 导出并开放
> https://gitee.com/lioncky/RE4/blob/master/Bio4/list.md

## 1.物品整合
| Hex  | No. | 名称         | Hex  | No. | 名称         | Hex  | No. | 名称         
|-------|--------|--------------|-------|--------|--------------|-------|--------|--------------|
| 0x00 | 00 | 麦林子弹| 0x64 | 100 | 啤酒杯黄| 0xC8 | 200 | 面具石红|
| 0x01 | 01 | 手雷弹  | 0x65 | 101 | 酒杯红绿| 0xC9 | 201 | 面具石紫|
| 0x02 | 02 | 燃烧弹  | 0x66 | 102 | 酒杯黄绿| 0xCA | 202 | 面具绿  |
| 0x03 | 03 | 玛提尔达| 0x67 | 103 | 酒杯红黄| 0xCB | 203 | 面具红  |
| 0x04 | 04 | 手枪子弹| 0x68 | 104 | 啤酒杯全| 0xCC | 204 | 面具紫  |
| 0x05 | 05 | 急救药  | 0x69 | 105 | 暮月石左| 0xCD | 205 | 面具绿红|
| 0x06 | 06 | 绿草    | 0x6A | 106 | 神器子弹| 0xCE | 206 | 面具绿紫|
| 0x07 | 07 | 步枪子弹| 0x6B | 107 | 准镜鸟狙| 0xCF | 207 | 面具红紫|
| 0x08 | 08 | 白蛋    | 0x6C | 108 | 准镜步枪| 0xD0 | 208 | 面具全色|
| 0x09 | 09 | 茶蛋    | 0x6D | 109 | 无限RPG | 0xD1 | 209 | 金猫像  |
| 0x0A | 10 | 金蛋    | 0x6E | 110 | 国王圣杯| 0xD2 | 210 | 审判之光|
| 0x0B | 11 | 未知物品| 0x6F | 111 | 王后圣杯| 0xD3 | 211 | 信义之光|
| 0x0C | 12 | 寄生样本| 0x70 | 112 | 至尊权杖| 0xD4 | 212 | 背信之光|
| 0x0D | 13 | 克劳泽刀| 0x71 | 113 | 金砖    | 0xD5 | 213 | 金猫像红|
| 0x0E | 14 | 闪光弹  | 0x72 | 114 | 普通箭矢| 0xD6 | 214 | 金猫像绿|
| 0x0F | 15 | 城主家徽| 0x73 | 115 | 时间奖励| 0xD7 | 215 | 金猫像蓝|
| 0x10 | 16 | 爆炸弩  | 0x74 | 116 | 3楼门卡 | 0xD8 | 216 | 金猫红绿|
| 0x11 | 17 | 爆炸箭  | 0x75 | 117 | 点数奖励| 0xD9 | 217 | 金猫蓝绿|
| 0x12 | 18 | 绿草二合| 0x76 | 118 | 绿猫眼石| 0xDA | 218 | 金猫红蓝|
| 0x13 | 19 | 绿草三合| 0x77 | 119 | 红宝石  | 0xDB | 219 | 金猫像全|
| 0x14 | 20 | 红绿合草| 0x78 | 120 | 宝箱小  | 0xDC | 220 | 瓶盖RPG |
| 0x15 | 21 | 三色合草| 0x79 | 121 | 宝箱大  | 0xDD | 221 | 瓶盖喷子|
| 0x16 | 22 | 黄绿合草| 0x7A | 122 | 暮月石全| 0xDE | 222 | 瓶盖手枪|
| 0x17 | 23 | 特殊RPG | 0x7B | 123 | 矿井钥匙| 0xDF | 223 | 盖碍事离|
| 0x18 | 24 | 喷子子弹| 0x7C | 124 | 小号背包| 0xE0 | 224 | 盖路易斯|
| 0x19 | 25 | 红草    | 0x7D | 125 | 中号背包| 0xE1 | 225 | 村民斧头|
| 0x1A | 26 | 手炮子弹| 0x7E | 126 | 大号背包| 0xE2 | 226 | 村民镰刀|
| 0x1B | 27 | 宝·沙漏 | 0x7F | 127 | 超大背包| 0xE3 | 227 | 村民徒手|
| 0x1C | 28 | 黄草    | 0x80 | 128 | 黄金宝剑| 0xE4 | 228 | 村民锄头|
| 0x1D | 29 | 拼图石板| 0x81 | 129 | 铁制钥匙| 0xE5 | 229 | 盖电锯男|
| 0x1E | 30 | 狮头石板| 0x82 | 130 | 牺牲之石|
| 0x1F | 31 | 山羊石板| 0x83 | 131 | 监禁门卡|
| 0x20 | 32 | TMP子弹 | 0x84 | 132 | 冷冻门卡|
| 0x21 | 33 | 惩罚者  | 0x85 | 133 | 黑豹浮雕|
| 0x22 | 34 | 惩罚子弹| 0x86 | 134 | 圣蛇浮雕|
| 0x23 | 35 | 初始手枪| 0x87 | 135 | 黑鹏浮雕|
| 0x24 | 36 | 消音手枪| 0x88 | 136 | 摩托钥匙|
| 0x25 | 37 | 驳壳红九| 0x89 | 137 | 脏珍珠链|
| 0x26 | 38 | 配件红九| 0x8A | 138 | 脏黄铜表|
| 0x27 | 39 | 黑尾手枪| 0x8B | 139 | 老式钥匙|
| 0x28 | 40 | 消音黑尾| 0x8C | 140 | 营地钥匙|
| 0x29 | 41 | 折翼蝴蝶| 0x8D | 141 | 爆破炸药|
| 0x2A | 42 | 杀手7   | 0x8E | 142 | 缆车钥匙|
| 0x2B | 43 | 消音杀手| 0x8F | 143 | 金手镯  |
| 0x2C | 44 | 霰弹枪  | 0x90 | 144 | 金香水瓶|
| 0x2D | 45 | 打击者  | 0x91 | 145 | 珍珠宝镜|
| 0x2E | 46 | 鸟狙    | 0x92 | 146 | 垃圾门卡|
| 0x2F | 47 | 狙击步枪| 0x93 | 147 | 西洋棋盘|
| 0x30 | 48 | TMP     | 0x94 | 148 | 防暴枪  |
| 0x31 | 49 | 战舰蓝匙| 0x95 | 149 | 黑鲈鱼  |
| 0x32 | 50 | 改装TMP | 0x96 | 150 | 宝石沙漏|
| 0x33 | 51 | 战舰红匙| 0x97 | 151 | 大黑鲈鱼|
| 0x34 | 52 | 里打字机| 0x98 | 152 | 邪恶宝石|
| 0x35 | 53 | 火箭筒  | 0x99 | 153 | 热瞄步枪|
| 0x36 | 54 | 地雷枪  | 0x9A | 154 | 凹槽王冠|
| 0x37 | 55 | 手炮    | 0x9B | 155 | 王冠宝石|
| 0x38 | 56 | 战刀    | 0x9C | 156 | 王冠勋章|
| 0x39 | 57 | 狮尾石板| 0x9D | 157 | 宝石王冠|
| 0x3A | 58 | 暮月石右| 0x9E | 158 | 徽章王冠|
| 0x3B | 59 | 村徽钥匙| 0x9F | 159 | 萨拉王冠|
| 0x3C | 60 | 教堂徽章| 0xA0 | 160 | 热瞄子弹|
| 0x3D | 61 | 村长义眼| 0xA1 | 161 | 祖母绿石|
| 0x3E | 62 | 汉克TMP | 0xA2 | 162 | A2瓶盖  |
| 0x3F | 63 | 手枪消音| 0xA3 | 163 | 画廊钥匙|
| 0x40 | 64 | 惩罚者  | 0xA4 | 164 | 徽章右  |
| 0x41 | 65 | PRL激光 | 0xA5 | 165 | 徽章左  |
| 0x42 | 66 | 红9枪托 | 0xA6 | 166 | 徽章全  |
| 0x43 | 67 | TMP枪托 | 0xA7 | 167 | 正城钥匙|
| 0x44 | 68 | 鸟狙瞄镜| 0xA8 | 168 | 红黄合草|
| 0x45 | 69 | 步枪瞄镜| 0xA9 | 169 | 村长宝图|
| 0x46 | 70 | 地雷子弹| 0xAA | 170 | 地雷瞄镜|
| 0x47 | 71 | Ada喷子 | 0xAB | 171 | 准地雷枪|
| 0x48 | 72 | 抓捕Luis| 0xAC | 172 | 游玩册01|
| 0x49 | 73 | 射击训练| 0xAD | 173 | Ashley册|
| 0x4A | 74 | Luis随笔| 0xAE | 174 | 游玩册02|
| 0x4B | 75 | 城主笔记| 0xAF | 175 | 村长密令|
| 0x4C | 76 | 有关Ada | 0xB0 | 176 | 蓝币玩法|
| 0x4D | 77 | 管家笔记| 0xB1 | 177 | 村长笔记|
| 0x4E | 78 | 已夺样本| 0xB2 | 178 | 封锁教会|
| 0x4F | 79 | 仪式准备| 0xB3 | 179 | 陌生的信|
| 0x50 | 80 | 路易斯02| 0xB4 | 180 | 游玩册3 |
| 0x51 | 81 | 热感步枪| 0xB5 | 181 | 第三组织|
| 0x52 | 82 | 克劳瑟弓| 0xB6 | 182 | 两条路线|
| 0x53 | 83 | 艾打字机| 0xB7 | 183 | 村末防线|
| 0x54 | 84 | 城堡宝图| 0xB8 | 184 | 蝴蝶灯  |
| 0x55 | 85 | 小岛宝图| 0xB9 | 185 | 绿蝇眼  |
| 0x56 | 86 | 蓝丝水晶| 0xBA | 186 | 红蝇眼  |
| 0x57 | 87 | 尖晶石  | 0xBB | 187 | 蓝蝇眼  |
| 0x58 | 88 | 珍珠吊坠| 0xBC | 188 | 蝴蝶灯绿|
| 0x59 | 89 | 黄铜怀表| 0xBD | 189 | 蝴蝶灯红|
| 0x5A | 90 | 萤石发饰| 0xBE | 190 | 蝴蝶灯蓝|
| 0x5B | 91 | 烟斗    | 0xBF | 191 | 蝶灯红绿|
| 0x5C | 92 | 五珠手环| 0xC0 | 192 | 蝶灯蓝绿|
| 0x5D | 93 | 琥珀戒指| 0xC1 | 193 | 蝶灯红蓝|
| 0x5E | 94 | 啤酒杯  | 0xC2 | 194 | 蝴蝶灯全|
| 0x5F | 95 | 绿猫眼石| 0xC3 | 195 | 牢房钥匙|
| 0x60 | 96 | 红猫眼石| 0xC4 | 196 | 白银宝剑|
| 0x61 | 97 | 黄猫眼石| 0xC5 | 197 | 热瞄镜  |
| 0x62 | 98 | 啤酒杯红| 0xC6 | 198 | 凹槽面具|
| 0x63 | 99 | 啤酒杯绿| 0xC7 | 199 | 面具石绿|

## 2.敌人ID属性整合
> UHD 与老版大致一致
> 

 No.| 名称| | 特殊|
 ---|---- |---|--- |
0 | 里昂| 1B 0A|瞎子
1 | 艾达| 1C 0A|瞎子
2 | -  | 1C 0D | 铁瞎子
3 | 艾什莉| 1D 02 | JJ机哥
4 | 路易斯| 1F 18 | 大锤哥
E | 小船 | 20 16| 老祖
F | 快艇| 44 1A | 夹克小哥
18 | **商人**| 13 06
21 | 狗子| 31 | 教主|	
22 | 恶狗| 32 | 三叔|	
23 | 乌鸦| 33 | -|	
24 | 蛇|	34 | -|	
25 | 乌鸦| 35 | 村长|	
26 | 阿牛| 36 | 橡皮|	
27 | 鲈鱼| 37 | -|	
28 | 芦鸡| 38 | 城主|	
29 | 蝙蝠| 39 | 老克|	
2A | 夹子| 3A | 机器|	
2B | 巨人| 3B | 卡车|	
2C | 右手| 3C | 骑士|	
2D | 飞虫| 3D | 麦克|	
2E | 蜘蛛| 3E | -|	
2F | 河主| 3F | 教主A|	

 整合| ID
 ---|-----
村民 | 10/12/13/15/16/17/42/44 
蓝小伙 | 12/13/15/17 01
白大爷 | 12/13/15/16/17 03
胖大叔 | 12/13/15/16 04
黄大妈 | 16/17 0C
玛莉亚 | 15/16 0B
教徒 | 11/14/19/1A/1B/1C
红衣 | 14/19/1A/1B 08
紫衣 | 14/19/1B/1C 09
士兵 | 1D/1E/1F/20
商人 | 18 / 13 06

## 3.特殊效果

> 爆虫位设置 则可能爆虫 不是必定爆虫

类型|1|2|4|8|10|20|40|80|
 ---|---|---|---|---|---|---|---|---
村民CLS1|深草帽|浅草帽|--|-|-|**无限道具**|小刀|-
村民CLS2|-|爆竹|头巾|舌帽|**爆虫**|眼镜|蓝妈头巾|蓝小伙帽
村民WEP|-|-|-|菜刀|电锯|一次性斧子|木桶(12/15)|叉子
-|--|-|-|-|-|-|-|-|-
教徒CLS1|羊角|铁头|-|火把|面罩|-|-|弩
教徒CLS2|-|爆竹|斗篷|-|**爆虫**|头纹|-|-
教徒WEP|-|-|镰刀|-|不腐|流星锤|火箭筒(1A)|木盾
-|--|-|-|-|-|-|-|-|-
士兵CLS1|深色夹克|钢盔|-|大尖锤|面罩|-|红头巾|弩
士兵CLS2|-|爆竹|头罩|劣质头盔|**爆虫**|劣质头盔|-|蓝色鸭舌帽
士兵WEP|-|-|电棒|斧子|电锯|流星锤|火箭筒|木盾


 No.|名称|适用
 ---|----|---
WEP7 | & 10 电锯 | 村民 士兵
WEP7 | & 20 斧子 | 村民
WEP7 | & 40 木桶 | 村民
WEP7 | & 80 叉子 | 村民
WEP7 | & 20 流星锤 | 教徒 士兵
WEP7 | & 40 火箭 | 教徒仅1A 士兵
WEP7 | & 80 木盾 | 教徒 士兵
flag4 | =8: 会盯着你


cmp byte ptr [eax+00004FC1],02
je 
bio4.exe+522CE8 - 81 88 20500000 00000004 - or [eax+00005020],04000000 { (0) } // 热妙境效果
bio4.exe+522CF2 - 81 8E 68040000 00020000 - or [esi+00000468],00000200 { 512 }

```

 0 em10_R1_move_tbl_4 [em10_R1_Wait]  @em10_R1_br_Dummy
 1 em10_R1_Keeper  @@
 2 em10_R1_Hide  @@
 3 em10_R1_HideFall  @@
 4 em10_R1_HideJump  @@
 5 em10_R1_R100TurnWalk  @@
 6 em10_R1_R100Cliff  @@
 7 em10_R1_R101Bucket  @@
 8 em10_R1_R101Suki  @@
 9 em10_R1_R101Cart em10_R1_br_EvtDash
 A em10_R1_EvtDash em10_R1_br_EvtWalk
 B em10_R1_EvtWalk  @@
 C em10_R1_Pickup  @@
 D em10_R1_Find  @@
 E em10_R1_C_SawStart  @@
 F em10_R1_BombIgnition em10_R1_br_Walk
10 em10_R1_Walk em10_R1_br_Dash
11 em10_R1_Dash em10_R1_br_Back
12 em10_R1_Back em10_R1_br_Goto
13 em10_R1_Goto  @@
14 em10_R1_GuardWalk  @@
15 em10_R1_Turn180  @@
16 em10_R1_Threat  @@
17 em10_R1_SideStep  @@
18 em10_R1_HideSide  @@
19 em10_R1_AppearSide  @@
1A em10_R1_SitDown  @@
1B em10_R1_Stay  @@
1C em10_R1_RoofWait  @@
1D em10_R1_Guard  @@
1E em10_R1_DownWakeWait  @@
1F em10_R1_DownWake  @@
20 em10_R1_ParasiteAtk  @@
21 em10_R1_ShotBowgun  @@
22 em10_R1_ShotRocket  @@
23 em10_R1_ShotGatling  @@
24 em10_R1_ThrowAxe  @@
25 em10_R1_ThrowBomb  @@
26 em10_R1_AxeAtk  @@
27 em10_R1_ShieldAtk  @@
28 em10_R1_TorchFrame  @@
29 em10_R1_SukiAtk  @@
2A em10_R1_ScytheAtk  @@
2B em10_R1_ClawAtk  @@
2C em10_R1_ClawWalkAtk em10_R1_br_ClawCriAtk
2D em10_R1_ClawCriAtk  @@
2E em10_R1_ClawCriHit em10_R1_br_C_SawAtk
2F em10_R1_C_SawAtk  @@
30 em10_R1_C_SawHit em10_R1_br_C_SawCriAtk
31 em10_R1_C_SawCriAtk  @@
32 em10_R1_C_SawCriHit em10_R1_br_Catch
33 em10_R1_Catch  @@
34 em10_R1_NeckHang  @@
35 em10_R1_NeckHang_Luis  @@
36 em10_R1_NeckHang_Ashley  @@
37 em10_R1_Backhold  @@
38 em10_R1_Bombhold em10_R1_br_DashCatch
39 em10_R1_DashCatch  @@
3A em10_R1_TakeAway  @@
3B em10_R1_Crash  @@
3C em10_R1_ClimbOver  @@
3D em10_R1_DoorAtk  @@
3E em10_R1_RackAtk  @@
3F em10_R1_WindowAtk  @@
40 em10_R1_LadderClimb  @@
41 em10_R1_VLadderClimb  @@
42 em10_R1_LadderReset  @@
43 em10_R1_JumpDown  @@
44 em10_R1_Jump  @@
45 em10_R1_JumpUp  @@
46 em10_R1_Trade  @@
47 em10_R1_Drive  @@
48 em10_R1_Catapult  @@
49 em10_R1_RockPush  @@
4A em10_R1_R10CParasite  @@
4B em10_R1_R10CPCancel  @@
4C em10_R1_R100WalkStay  @@
4D em10_R1_R202Finger  @@
4E em10_R1_StayWalk  @@
4F em10_R1_AttackWait  @@
50 em10_R1_FixBomber  @@
51 em10_R1_R204Prayer  @@
52 em10_R1_R222DragonA  @@
53 em10_R1_R222DragonB  @@
54 em10_R1_R222DragonC  @@
55 em10_R1_R227Barrel  @@
56 em10_R1_R10FGJump  @@
57 em10_R1_R10FGondola  @@
58 em10_R1_R209DashSit  @@
59 em10_R1_StickClaw  @@
5A em10_R1_R11DAppear1  @@
5B em10_R1_R11DAppear2  @@
5C em10_R1_R212Drill  @@
5D em10_R1_FindLost  @@
5E em10_R1_R201EventWait  @@
5F em10_R1_R209Gatling  @@
60 em10_R1_RocketWait  @@
61 em10_R1_R21BTrolleyJump  @@
62 em10_R1_R21BTrolleyJump2  @@
63 em10_R1_R303FireDash  @@
64 em10_R1_Work  @@
65 em10_R1_UFOCatch  @@
66 em10_R1_R300TakeAshley  @@
67 em10_R1_R30FBullJump  @@
68 em10_R1_R320Gatling  @@
69 em10_R1_R300Gatling  @@
6A em10_R1_R305Bomber  @@
6B em10_R1_R321DeadBody  @@
6C em10_R1_R408Bomber em10_R1_br_CSawWalkAtk
6D em10_R1_CSawWalkAtk  @@
6E em10_R1_R518Gatling_mb
```
















<!--stackedit_data:
eyJoaXN0b3J5IjpbMjE0ODY4Nzc0LC0xNzc3NjM5MDk4LDE3NT
I3Mjc5ODksLTU4NDcwNDUyMCwtNDg5NjI4NzM3LDIzMTIwNjQ3
MCwxMDcxMTQ4MjksLTEwMzYzODYxNzAsLTE1NjUyMjI0NTgsMT
U3NTA1ODE3NSwzNzk0NTgxNzUsLTM4MTI5OTU4LC0xOTM1NjI4
MDYzLC0xMTI2NjU5NTEzLDE3NjU5ODgyMTYsLTE1NjkwNjI0ND
ksLTYwNTczODQyNSwyMjc2MDU4NTgsMjA2MjU5NzY3Miw0MTQz
OTMxNzBdfQ==
-->