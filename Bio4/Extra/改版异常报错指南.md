# RE4HD的改版避坑指南

## 1. 4GB not Patched!
```
这个问题主要出现于改版之上 一般只有ArrangeMod(万人斩)会报这个错误
解决方案是使用群里的4gb双版本补丁 然后将bio4106FIX复制到bin32
删除原本的bio4.exe 然后将bio4106FIX改为bio4 启动游戏即可
此时一些替换启动器的mod将失去效果 
比如一些很旧版本的3倍刀2倍体术等等
但是BT系列高版本启动器是处理过的 可以直接使用
```
![输入图片说明](/imgs/2023-12-02/ZiDcDv7ZjlHOZnuw.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3MTgxOTc1NzFdfQ==
-->