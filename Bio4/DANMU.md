
![](https://gitee.com/lioncky/RE4/raw/master/imgs/vagtMT.png)
# [远程弹幕生成敌人活跃模块](https://gitee.com/lioncky/RE4/blob/master/Bio4/DANMU.md)
## 条件
- [x] 1.一个自己的Q号
- [x] 2.一个blibli直播间账号
- [x] 3.使用倾龙时登录自己的Q号
- [x] 4.在直播间使用 刷怪指令 即可
- [x] 5.倾龙EOE引擎版本要求v3917+

## 指令
受支持的弹幕指令
来发+补给名称 如 绿草 rpg
来个+怪物名称 如 老祖 老克 橡皮

生成指令一览|A指令X|A指令X|A指令X|A指令X|A指令X|A指令X
---|---|---|---|---|---|---
a | '老祖' |'狗子' |'铁瞎' | '二翅' | '机哥'| '碍事'
a | '铁祖' | '夹子' |'锤哥' | '机器' | '铁机'| '哥哥'
a | '火祖' | '夹夹' |'铁锤' | '橡皮' | - | '班班'
a | '铁火' | 'd雷' |'火锤' | '尖刺' | '斧哥' |
a | '电锯' | '巨人' |'右手' | '三叔' | '弩哥' |
a | '电姐' | '铁巨' |'老克' | '村长' | '爆哥' |
a | '电妹' | '瞎子' |'鸡翅' | '教主' | '铁弩' |

> 哥哥->路y易斯 班班->商人 铁机->铁头机哥
> 火祖->爆竹老祖 铁火->铁头爆竹老祖
> NPC:  '大人' '城主' '黑袍' '红袍'
> 例子： 来个老祖 32000
> 

生成补给| 生成枪
---|---
|'来发RPG'|'来把蝴蝶'
|'来发火箭'|'来把手炮'
|'来发特殊'|'来把红九'
|'来发三合'|'来把重喷'
|'来发哈哈'|'来把蝴蝶'
|'来发绿草'|'来把鸟狙'
|'来发黄草'|'来把连狙'
|'来发红草'|'来把TMP'
|'来发d4雷'|'来把d4雷'
|'来发麦林'|'来把麦林'
|*-*|
|'来发shou(雷'|
|'来发燃烧'|
|'来发闪光'|
|'来发消音'|
|'来发热瞄'|

> 例子：来把重喷
> 短暂网络延迟后 主播将获得打击者
> 

![](/imgs/2024-03-29/700bNlpL6CvPzmQz.png)

> [刷怪演示 https://www.bilibili.com/video/BV1yD421W7np](https://www.bilibili.com/video/BV1yD421W7np) <br>
> 建议反馈群 1057728030 <br>
> 改版交流群 419475804 <br>
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk0MzU3Nzc4Nyw1MzAwODYzOTQsMTk0MD
Y4MzM3NSwtMjgwNTYyNzc5LC0xMTc5MzkwOTQxLC0xMTQ0NzU3
ODE5LC02NjkzMTkxODIsLTE0Nzk3Njk4OTAsMTY2NDk3OTY2Ni
w2MTEyNjgyMzIsNjA1OTAwMzA5LDYyNzY2NzY4OSwtMTcwMDUz
MTYzMywxNzQwOTQ4OTk3LC03ODAwODk2OTMsLTE4NTIwNzQ0Nj
MsLTk1NzQxOTE3NywtMTg0NTI5NTMzOCwtMTgwNDk2NTA1Nywt
Mjc1NTk4NTgyXX0=
-->