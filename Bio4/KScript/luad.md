
```lua
local fa = fa
local js = json
local imgui = imgui
local function ksend(_) return KS_Send(_,#_) end
local function ZWB(_os,_) return _ZWB(_os,_,#_) end
-- local WindowFlags = WindowFlags

local VK_HOME = 36
local ic_red = 0xFF0000FF

local mk
local b106 = false
local md_mod, md_gsl, md_dev, md_log, md_danmu = '', '', '', '', ''
local md_kscript_ver = ''

local a = 1
local g_dbg = true
function lua_render_ks()
	imgui.same_line()
    if imgui.small_button('X') then
	    KS_Stop();
    end
	imgui.same_line()
	imgui.text(b106 and 'ver:1.0.6' or 'ver:1.1.0') 
    
	imgui.same_line()
    if imgui.small_button('刷新云脚本') then
	    KS_Reboot();
    end
	imgui.same_line()
    if imgui.small_button('test') then
	    ksend('OP=USERDATA');
    end
    
	imgui.same_line()
    if imgui.small_button('测试PARSE JSON') then
        local j = js.load_string('{"aaa":{"ccc":9888}}')
        local flags = j['aaa']['ccc']
        -- local flags = imgui.WindowFlags.None + imgui.ColumnFlags.DefaultHide
        imgui.center_tips_add(string.format("%d", flags), ic_red)
    end
    
	imgui.same_line()
    if imgui.button('+1') then
        a = a + 1
    end
	imgui.same_line()
    imgui.text(string.format("%d", a))

	imgui.same_line()
    if imgui.small_button('KSock_OpenMd') and #md_kscript_ver>0 then
        local j =  js.load_string(md_kscript_ver)
        local date = j['date']
        imgui.center_tips_add(date, ic_red)
    end
    if #md_mod > 0 then
	    if imgui.collapsing_header("GS引擎：倾龙改版整合页 (mod.re4gs.cn)") then
		    imgui_markdown(md_mod, #md_mod)
	    end
    end
    if #md_danmu > 0 then
	    if imgui.collapsing_header("GS引擎：弹幕刷怪一览 (www.re4gs.cn)") then
		    imgui_markdown(md_danmu, #md_danmu)
	    end
    end
    if #md_log> 0 then
	    if imgui.collapsing_header("GS引擎：3.X日志 (www.re4gs.cn)") then
		    imgui_markdown(md_log,#md_log)
	    end
    end
    if #md_dev> 0 then
	    if imgui.collapsing_header("GS引擎：开发者 (dev.re4gs.cn)") then
		    imgui_markdown(md_dev,#md_dev)
	    end
    end
    if #md_gsl> 0 then
	    if imgui.collapsing_header("GS引擎：ESL (www.re4gs.cn)") then
		    imgui_markdown(md_gsl,#md_gsl)
	    end
    end
    if false and imgui.collapsing_header("FontAwesome-v5") then
        local i = 0

        local size = imgui.calc_text_size(fa['ad'] .. "测试测试测试")
        -- imgui.text(string.format("%.1f", size.x))
        size = size * 1.2
        for key, value in pairs(fa) do
            
            if imgui.button2(value .. key, size) then
	            local name = 'ICON_FA_' .. string.upper(key)
                imgui.set_clipboard(name )
                imgui.center_tips_add(name , ic_red)
            end
            imgui.cover_tips(value .. ' ICON_FA_' .. string.upper(key))

            i = i + 1
            if i < 4 then
                imgui.same_line()
            else
                i = 0
            end
        end    
	 end
end

function lua_render_main()
	--show_demo_wnd()
	if GetAsyncKeyState(VK_HOME) and not g_dbg then
		g_dbg = true 
	end
	
    if not imgui.begin_window(fa['acorn'] .. 'KSA_DBG', g_dbg, 0) then
        if g_dbg then
            g_dbg = false
            imgui.end_window()
        end
        return
    end
    
    lua_render_ks();
    imgui.end_window() 
end

function lua_sock_recv(data)
	KS_O('lua' .. data)
	
    local map = {}
    -- 使用 '|' 作为分隔符，拆分字符串
    if false then
    for pair in string.gmatch(data, "([^|]+)") do
        -- 按照 '=' 分割每个 key=value
        local key, value = pair:match("([^=]+)=([^=]+)")
        if key and value then
            map[key] = value  -- 存入 Lua 表
        else
            print("Error parsing: " .. pair)
        end
    end
	
	if map['OP'] then imgui.center_tips_add(map['OP'], ic_red) end
	end
end

function lua_thread_init()
	b106 = QZS(0x100000) == 0x000002A8
	md_mod = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/MOD.md')
	md_dev = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/Dev.md')
	md_log = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/LOG.md')
	md_gsl = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/GSL.md')
	md_danmu = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/DANMU.md') 
	md_kscript_ver = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/KScript/ver.md')
end
--DEADBEEF
```
[luaURL](https://gitee.com/lioncky/RE4/raw/master/Bio4/KScript/lua.md)

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTcxMzc3NzEyMiwtMTkzNjQwMjYyN119
-->