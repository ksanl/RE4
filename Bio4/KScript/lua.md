```lua
local fa = fa
local js = json
local imgui = imgui
local function ksend(_) return KS_Send(_,#_) end
local function ZWB(_os,_) return _ZWB(_os,_,#_) end
-- local WindowFlags = WindowFlags

local VK_HOME = 36
local ic_red = 0xFF0000FF

local mk
local img_test
local b106 = false

local game_ver, kscript_ver = '',''
local md_mod, md_gsl, md_dev, md_log, md_danmu = '', '', '', '', ''

local gG, usr_total, usr_cur = 0, 0, 0
local g_dbg = true

local g_em_spawn_ct, g_em_spawn_height, g_em_spawn_hp = 1,1,8888

local function render_gs_markdown(str, md_text) if md_text ~= nil and #md_text> 0 then if imgui.collapsing_header(str) then imgui_markdown(md_text, #md_text) end end end
local function get_game_state() return QZJ(gG + 0x20) end
local function get_room_id() return QDZ(gG + 0x4FAC) end
local function get_player_id() return QZJ(gG + 0x4FC8) end

local a = 1
function lua_render_ks()
	imgui.same_line()
    if imgui.button(fa['redo']) then
	    KS_Reboot(); return;
    end imgui.cover_tips('刷新云脚本')
    
	imgui.same_line()
    if imgui.small_button(fa['user']) then ksend('OP=USERDATA') end imgui.cover_tips('刷新用户数据')
    
	imgui.same_line()
	imgui.text(string.format('KScript:v%s ver:%s r%04X %d',kscript_ver, game_ver, get_room_id(),get_game_state())) 
    
	imgui.same_line()
    if imgui.button('+1') then a = a + 1 end
    
	imgui.same_line()
    imgui.text(string.format("%d", a))
	
	imgui.same_line()
    if imgui.small_button('X') then KS_Stop(); end imgui.cover_tips('游戏将极速关闭')

	-- AERA_BAR_END
	local normal = usr_total < 598
	local is_gaming = get_game_state() == 3
	if imgui.collapsing_header(normal and '普通-生成敌人' or '至尊-生成敌人') and is_gaming then 
		if normal then 
			imgui.text(fa['lock_alt'] .. '基础版:生成倍率:1 生成血量:8888')
			if imgui.small_button('普通-来个电锯') then KS_Spawn(0x15,1,8888,0x10,0,0,1,1)
			elseif imgui.small_button('普通-来个路易斯') then KS_Spawn(0x4,1,8888,0x0,0,0,1,1)
			end imgui.cover_tips('[3.15截止]赞助达到598/击杀数达到598000时可解锁 至尊-生成敌人 极致完美版 附带倍率血量高度等细分\n3.16后不可获得 已赞助够数的可免费领取 兑换剩余:2[月初狂欢:5%上报计算为10000]')
		else
		
		 local h = g_em_spawn_height
		 imgui.begin_group()
		  if imgui.button('老祖') then KS_Spawn(0x20,0x16,g_em_spawn_hp,0x10,0,0,h,g_em_spawn_ct) end
		  if imgui.button('电锯') then KS_Spawn(0x20,1,g_em_spawn_hp,0x10,0,0,h,g_em_spawn_ct) end  
		  if imgui.button('老克') then KS_Spawn(0x39,0,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('鸡翅') then KS_Spawn(0x39,2,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('巨人') then KS_Spawn(0x2B,3,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		 imgui.end_group()
		 
		 imgui.same_line()
		 imgui.begin_group()
		  if imgui.button('右手') then KS_Spawn(0x2C,0,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('瞎子') then KS_Spawn(0x1B,10,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('铁瞎') then KS_Spawn(0x1C,10,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('教主') then KS_Spawn(0x3F,00,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('三叔') then KS_Spawn(0x32,00,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		 imgui.end_group()
		 
		 imgui.same_line()
		 imgui.begin_group()
		  if imgui.button('艾什莉') then KS_Spawn(0x3,1,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('路易斯') then KS_Spawn(0x4,1,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('JJ机哥') then KS_Spawn(0x1D,2,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('橡皮人') then KS_Spawn(0x36,0,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		  if imgui.button('刺皮人') then KS_Spawn(0x36,3,g_em_spawn_hp,0,0,0,h,g_em_spawn_ct) end
		 imgui.end_group()
		 
		 imgui.same_line()
		 imgui.begin_group()
		  _, g_em_spawn_hp = imgui.slider_int('血量',g_em_spawn_hp,1,32767,'%d') -- set hp
		  _, g_em_spawn_ct = imgui.slider_int('倍率',g_em_spawn_ct,1,10,'%d') -- set count
		  _, g_em_spawn_height = imgui.slider_int('高度',g_em_spawn_height,1,12000,'%d') -- set h
		 imgui.end_group()
		end
	end
	
	--- Function_end
    render_gs_markdown("GS引擎：倾龙改版整合页 (mod.re4gs.cn)",md_mod)
    --render_gs_markdown("GS引擎：弹幕刷怪一览 (www.re4gs.cn)",md_danmu)
    --render_gs_markdown("GS引擎：威虎3.X日志 (www.re4gs.cn)",md_log)
    --render_gs_markdown("GS引擎：开发者 (dev.re4gs.cn)",md_dev)
    --render_gs_markdown("GS引擎：ESL (dev.re4gs.cn)",md_gsl)
    if img_test ~= nil then imgui.image(img_test,800,200) end
    
    if false and imgui.collapsing_header("FontAwesome-v5") then
        local i = 0

        local size = imgui.calc_text_size(fa['ad'] .. "测试测试测试")
        -- imgui.text(string.format("%.1f", size.x))
        size = size * 1.2
        for key, value in pairs(fa) do
            
            if imgui.button2(value .. key, size) then
	            local name = 'ICON_FA_' .. string.upper(key)
                imgui.set_clipboard(name)
                imgui.center_tips_add(name, ic_red)
            end
            imgui.cover_tips(value .. ' ICON_FA_' .. string.upper(key))

            i = i + 1
            if i < 4 then
                imgui.same_line()
            else
                i = 0
            end
        end    
	 end
end

function lua_render_main() end
function lua_loop_main() end

function lua_sock_recv(_)
	KS_O('[lua]' .. _)
	
    local data = {}
    local opcode = ''
    for pair in string.gmatch(_, "([^|]+)") do
        local key, value = pair:match("([^=]+)=([^=]+)")
        if key and value then
            data[key] = value
        end
    end
    
    if data['USERDATA'] then -- USERDATA|TOTAL|CUR
	    local USERDATA	= data['USERDATA']
	    local TOTAL		= data['TOTAL']
	    local CUR		= data['CUR']
	    usr_total = tonumber(TOTAL) or 0
	    usr_cur = tonumber(CUR) or 0
	    ZZS(0x600,usr_flag)
	    ZZS(0x604,usr_id)
	    ZZS(0x608,usr_total)
	    ZZS(0x60C,usr_cur)
	    return
    end 
    
	-- OPCODE
	local OP = data['OP']
    if OP ~= nil then --
	    
		if OP == 'Stop' then
			KS_Stop()
		end
		
	    return
    end
end

function lua_thread_init()
	b106 = QZS(0x100000) == 0x000002A8; 
	game_ver = b106 and '1.0.6' or '1.1.0'; gG = b106 and 0x856EE0 or 0x85A760
	
	md_mod = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/MOD.md')
	--md_dev = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/Dev.md')
	--md_log = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/LOG.md')
	--md_gsl = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/GSL.md')
	--md_danmu = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/DANMU.md') 

	local s = KSock_OpenMd('https://gitee.com/lioncky/RE4/raw/master/Bio4/KScript/ver.md')
	if #s>0 then
        local j = js.load_string(s)
        if j ~= nil then kscript_ver = j['ver'] end
    end
    
	if KS_LoadImageUrl then -- KS 4020
		XSleep(500)
		img_test = KS_LoadImageUrl('https://gitee.com/lioncky/RE4/raw/master/imgs/vagtMT.png')
	end
end
--DEADBEEF
```
[luaURL](https://gitee.com/lioncky/RE4/raw/master/Bio4/KScript/lua.md)
```lua
	imgui.same_line()
    if imgui.small_button('KSock_OpenMd') and #md_kscript_ver>0 then
        local j =  js.load_string(md_kscript_ver)
        local ver = j['ver']
        imgui.center_tips_add('KScript v' .. ver .. ' ', ic_red)
    end

	imgui.same_line()
    if imgui.small_button('测试PARSE JSON') then
        local j = js.load_string('{"aaa":{"ccc":9888}}')
        local flags = j['aaa']['ccc']
        -- local flags = imgui.WindowFlags.None + imgui.ColumnFlags.DefaultHide
        imgui.center_tips_add(string.format("%d", flags), ic_red)
    end
	    
	function lua_render_main()
		--show_demo_wnd()
		if GetAsyncKeyState(VK_HOME) and not g_dbg then
			g_dbg = true 
		end
		
	    if not imgui.begin_window(fa['acorn'] .. 'KSA_DBG', g_dbg, 0) then
	        if g_dbg then
	            g_dbg = false
	            imgui.end_window()
	        end
	        return
	    end
	    
	    lua_render_ks()
	    imgui.end_window()
	end

		if usr_total < 698 then imgui.text(fa['lock_alt'] .. '当前: 基础版本') 
		else imgui.text(fa['lock_open'] .. '当前: 专业版本') end
		
		local b, g_em_cur = imgui.combo("选择一个选项", g_em_cur, g_em_names)
		if b then 
			local cur = tostring(g_em_names[g_em_cur])
			imgui.center_tips_add(cur, ic_red)
		   --KS_Spawn(0x15,1,2222,0x10,0,0,1,2)
		end
		imgui.same_line()
	    if imgui.small_button('t') then 
		   KS_Spawn(0x15,1,2222,0x10,0,0,1,2)
		end imgui.cover_tips('TEST')
	    if imgui.small_button('item') then 
		   KS_SpawnItem(0x1,1)
		end imgui.cover_tips('TEST')
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTYxMjcyOTYwOSw2MTQzODA3OTQsLTIxMT
c0NjU3NjEsMTY1ODg1Nzk0MCwxMDE0MjIwNTkxLC0xNzY4NzIx
Mzc0LDY5MDI4MTYwNiwxMDE4NDE2MzkxLDM1NzcxMTU4MCwtMj
EzMTQ5MTY0NywyNzc0MjcxNzAsMzM4MjY2NzY3LC03NTE4NzU3
MTgsMTM0OTExODcxOSwyMTU5NDg4NzAsOTQ5NzQyOTE1LC00Nj
M2OTY2MzUsLTc5MzA2ODksMzY4Mzk4MDEsLTE5NDUyMjM1MjFd
fQ==
-->