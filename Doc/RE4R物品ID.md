> 本数据由 [3DM盖世班主](https://bbs.3dmgame.com/home.php?mod=space&uid=17153690&do=thread) 开源并导出 感谢大家支持盖世引擎~
> [RE4R物品ID.md · KSA-NL/XCS2022 - 码云 - 开源中国](https://gitee.com/ksanl/xcs2022/blob/master/Docs%E6%96%87%E6%A1%A3/RE4R%E7%89%A9%E5%93%81ID.md)
> 最后更新于2023.10.1 Update for Ada DLC
> 
ID |类型| 内部| 名称
---|---|---|---
0x06B93100|Ammo       |it_sm70_500_00_0|手强子弹
0x06B93740|Ammo       |it_sm70_501_00_0|马格南子弹
0x06B93D80|Ammo       |it_sm70_502_00_0|喷子子弹
0x06B943C0|Ammo       |it_sm70_503_00_0|步强子弹
0x06B94A00|Ammo       |it_sm70_504_00_0|冲锋强子弹
0x06B95040|Ammo       |it_sm70_505_00_0|弩1箭
0x06B95680|Ammo       |it_sm70_506_00_0|
0x06B95CC0|Ammo       |it_sm70_507_00_0|
0x06B96300|Ammo       |it_sm70_508_00_0|
0x06B96940|Ammo       |it_sm70_509_00_0|
0x06B44F00|Ammo       |it_sm70_300_00_0|【炸1药2箭】
0x06B1DE00|Ammo       |it_sm70_200_00_0|
0x06CCB900|Hurb       |it_sm71_300_00_0|【格雷戈里奥水道之主】
0x06D19B00|Hurb       |it_sm71_500_00_0|绿色草药
0x06D19BA0|Hurb       |it_sm71_500_10_0|
0x06D1A140|Hurb       |it_sm71_501_00_0|红色草药
0x06D1A1E0|Hurb       |it_sm71_501_10_0|
0x06D1A780|Hurb       |it_sm71_502_00_0|黄色草药
0x06D1ADC0|Hurb       |it_sm71_503_00_0|混合草药（绿+绿）
0x06D1B400|Hurb       |it_sm71_504_00_0|混合草药（绿+绿+绿）
0x06D1BA40|Hurb       |it_sm71_505_00_0|混合草药（绿+红）
0x06D1C080|Hurb       |it_sm71_506_00_0|混合草药（绿+黄）
0x06D1C6C0|Hurb       |it_sm71_507_00_0|混合草药（红+黄）
0x06D1CD00|Hurb       |it_sm71_508_00_0|混合草药（绿+红+黄）
0x06D1D340|Hurb       |it_sm71_509_00_0|混合草药（绿+绿+黄）
0x06D1D980|Hurb       |it_sm71_510_00_0|急救喷雾
0x06D1DFC0|Hurb       |it_sm71_511_00_0|黑鲈鱼
0x06D1E600|Hurb       |it_sm71_512_00_0|大鲈鱼
0x06D1EC40|Hurb       |it_sm71_513_00_0|黑鲈鱼（大）
0x06D1F280|Hurb       |it_sm71_514_00_0|蝰蛇
0x06D1F8C0|Hurb       |it_sm71_515_00_0|独角仙
0x06EA0500|Parts      |it_sm72_500_00_0|瞄准镜
0x06EA0B40|Parts      |it_sm72_501_00_0|强托（Red9）
0x06EA1180|Parts      |it_sm72_502_00_0|高倍瞄准镜
0x06EA17C0|Parts      |it_sm72_503_00_0|生物传感镜
0x06EA1E00|Parts      |it_sm72_504_00_0|强托（TMP）
0x06EA2440|Parts      |it_sm72_505_00_0|激光瞄1准器
0x06EA2A80|Parts      |it_sm72_506_00_0|强托（玛蒂尔达）
0x07026F00|Powder     |it_sm73_500_00_0|火药
0x07027540|Powder     |it_sm73_501_00_0|物资（大）
0x07027B80|Powder     |it_sm73_502_00_0|弩1见雷
0x070281C0|Powder     |it_sm73_503_00_0|破损的小刀
0x07028800|Powder     |it_sm73_504_00_0|物资（小）
0x071AE580|Key        |it_sm74_502_00_0|六边形浮雕
0x071AEBC0|Key        |it_sm74_503_00_0|徽记钥匙
0x071AF200|Key        |it_sm74_504_00_0|光晕转轮
0x071AFE80|Key        |it_sm74_506_00_0|绯红提灯
0x071B04C0|Key        |it_sm74_507_00_0|1级钥匙卡
0x071B0B00|Key        |it_sm74_508_00_0|2级钥匙卡
0x071B1140|Key        |it_sm74_509_00_0|3级钥匙卡
0x071B1DC0|Key        |it_sm74_511_00_0|水晶球
0x071B2400|Key        |it_sm74_512_00_0|扳手
0x071B2A40|Key        |it_sm74_513_00_0|萨拉扎家族徽记
0x071B3080|Key        |it_sm74_514_00_0|狮头雕像
0x071B36C0|Key        |it_sm74_515_00_0|羊头雕像
0x071B3D00|Key        |it_sm74_516_00_0|蛇头雕像
0x071B4FC0|Key        |it_sm74_519_00_0|亵1渎者头雕
0x071B5600|Key        |it_sm74_520_00_0|叛1教者头雕
0x071B6280|Key        |it_sm74_522_00_0|木质齿轮
0x071B68C0|Key        |it_sm74_523_00_0|开矿炸2-药
0x071B6F00|Key        |it_sm74_524_00_0|路易斯的钥匙
0x071B8800|Key        |it_sm74_528_00_0|小钥匙
0x071B8E40|Key        |it_sm74_529_00_0|关隘大门摇柄
0x071B9480|Key        |it_sm74_530_00_0|六边形拼图A
0x071B9AC0|Key        |it_sm74_531_00_0|六边形拼图B
0x071BA100|Key        |it_sm74_532_00_0|六边形拼图C
0x071BAD80|Key        |it_sm74_534_00_0|地牢钥匙
0x071BB3C0|Key        |it_sm74_535_00_0|老旧的神龛钥匙
0x071BBA00|Key        |it_sm74_536_00_0|教堂徽记
0x071BC040|Key        |it_sm74_537_00_0|摩托艇钥匙
0x071BC680|Key        |it_sm74_538_00_0|金剑
0x071BCCC0|Key        |it_sm74_539_00_0|铁剑
0x071BD940|Key        |it_sm74_541_00_0|版画石板A
0x071BDF80|Key        |it_sm74_542_00_0|版画石板B
0x071BE5C0|Key        |it_sm74_543_00_0|版画石板C
0x071BEC00|Key        |it_sm74_544_00_0|版画石板D
0x071BF880|Key        |it_sm74_546_00_0|小艇燃料
0x071BFEC0|Key        |it_sm74_547_00_0|钥匙串
0x071C0500|Key        |it_sm74_548_00_0|立方雕
0x071C0B40|Key        |it_sm74_549_00_0|血剑
0x071C1180|Key        |it_sm74_550_00_0|锈剑
0x071C17C0|Key        |it_sm74_551_00_0|猎人小屋的钥匙
0x071C1E00|Key        |it_sm74_552_00_0|独角兽之角
0x071C2440|Key        |it_sm74_553_00_0|蓝色转盘
0x071C2A80|Key        |it_sm74_554_00_0|银质代币
0x071C30C0|Key        |it_sm74_555_00_0|金质代币
0x071C3700|Key        |it_sm74_556_00_0|木板
0x071C3D40|Key        |it_sm74_557_00_0|
0x0715F700|Key        |it_sm74_300_00_0|
0x0715FD40|Key        |it_sm74_301_00_0|
0x07160380|Key        |it_sm74_302_00_0|
0x071609C0|Key        |it_sm74_303_00_0|
0x07161000|Key        |it_sm74_304_00_0|
0x07161640|Key        |it_sm74_305_00_0|【红色油墨】
0x07161C80|Key        |it_sm74_306_00_0|【黄金酒瓶】
0x071622C0|Key        |it_sm74_307_00_0|【蓝色蝴蝶】
0x07162900|Key        |it_sm74_308_00_0|
0x07162F40|Key        |it_sm74_309_00_0|
0x07163580|Key        |it_sm74_310_00_0|
0x07163BC0|Key        |it_sm74_311_00_0|
0x07164200|Key        |it_sm74_312_00_0|
0x07164840|Key        |it_sm74_313_00_0|
0x07164E80|Key        |it_sm74_314_00_0|
0x071654C0|Key        |it_sm74_315_00_0|【RPG（特殊）】
0x07165B00|Key        |it_sm74_316_00_0|【快艇钥匙】
0x07166140|Key        |it_sm74_317_00_0|【白银酒瓶】
0x07166780|Key        |it_sm74_318_00_0|
0x07166DC0|Key        |it_sm74_319_00_0|
0x07167400|Key        |it_sm74_320_00_0|【版画石板A】
0x07167A40|Key        |it_sm74_321_00_0|【版画石板B】
0x07168080|Key        |it_sm74_322_00_0|【版画石板C】
0x071686C0|Key        |it_sm74_323_00_0|【版画石板D】
0x07168D00|Key        |it_sm74_324_00_0|
0x07169340|Key        |it_sm74_325_00_0|【隐藏钥匙】
0x07169980|Key        |it_sm74_326_00_0|【青月石雕】
0x07169FC0|Key        |it_sm74_327_00_0|【供电单元】
0x072E6100|Treasure   |it_sm75_300_00_0|【夹克】
0x072E6740|Treasure   |it_sm75_301_00_0|
0x072E6D80|Treasure   |it_sm75_302_00_0|【贝雷帽】
0x072E73C0|Treasure   |it_sm75_303_00_0|【管家的发夹】
0x072E7A00|Treasure   |it_sm75_304_00_0|【眼镜】
0x07334300|Treasure   |it_sm75_500_00_0|尖晶石
0x07334940|Treasure   |it_sm75_501_00_0|珍珠吊坠
0x07334F80|Treasure   |it_sm75_502_00_0|脏臭的珍珠吊坠
0x07336880|Treasure   |it_sm75_506_00_0|黄铜怀表
0x07337500|Treasure   |it_sm75_508_00_0|萤石发饰
0x07337B40|Treasure   |it_sm75_509_00_0|老式烟斗
0x07338180|Treasure   |it_sm75_510_00_0|珍珠手镯
0x073387C0|Treasure   |it_sm75_511_00_0|红宝石戒指
0x07338E00|Treasure   |it_sm75_512_00_0|金手镯
0x07339440|Treasure   |it_sm75_513_00_0|光明吊坠
0x07339A80|Treasure   |it_sm75_514_00_0|珍珠红宝石镜
0x0733A0C0|Treasure   |it_sm75_515_00_0|金沙漏
0x0733A700|Treasure   |it_sm75_516_00_0|优雅的香水瓶
0x0733AD40|Treasure   |it_sm75_517_00_0|华美的棋盘
0x0733B380|Treasure   |it_sm75_518_00_0|皇家权杖
0x0733B9C0|Treasure   |it_sm75_519_00_0|红宝石
0x0733C000|Treasure   |it_sm75_520_00_0|祖母绿
0x0733C640|Treasure   |it_sm75_521_00_0|蓝宝石
0x0733CC80|Treasure   |it_sm75_522_00_0|黄钻石
0x0733D900|Treasure   |it_sm75_524_00_0|亚历山大石
0x0733DF40|Treasure   |it_sm75_525_00_0|金条
0x0733E580|Treasure   |it_sm75_526_00_0|金条（大）
0x0733EBC0|Treasure   |it_sm75_527_00_0|金砖
0x0733F200|Treasure   |it_sm75_528_00_0|堕落神像
0x0733F840|Treasure   |it_sm75_529_00_0|老式指南针
0x0733FE80|Treasure   |it_sm75_530_00_0|正义女神像
0x073404C0|Treasure   |it_sm75_531_00_0|水晶矿石
0x07340B00|Treasure   |it_sm75_532_00_0|华丽的甲虫
0x07341140|Treasure   |it_sm75_533_00_0|门德斯的义眼
0x07341780|Treasure   |it_sm75_534_00_0|金丝单眼镜
0x07341DC0|Treasure   |it_sm75_535_00_0|胭脂口红
0x07342400|Treasure   |it_sm75_536_00_0|红色线柱石
0x07343080|Treasure   |it_sm75_538_00_0|老式相机
0x07343D00|Treasure   |it_sm75_540_00_0|紫馨石
0x07344340|Treasure   |it_sm75_541_00_0|刮花的祖母绿
0x07344980|Treasure   |it_sm75_542_00_0|高级改造券
0x0735B400|Treasure   |it_sm75_600_00_0|
0x074BAD00|Combinable |it_sm76_500_00_0|奢华的面具
0x074BB340|Combinable |it_sm76_501_00_0|酒壶
0x074BB980|Combinable |it_sm76_502_00_0|妖光蝶提灯
0x074BBFC0|Combinable |it_sm76_503_00_0|高贵的王冠
0x074BC600|Combinable |it_sm76_504_00_0|金山猫像
0x074BCC40|Combinable |it_sm76_505_00_0|豪华的时钟
0x074BD280|Combinable |it_sm76_506_00_0|华贵的手镯
0x074BD8C0|Combinable |it_sm76_507_00_0|救赎圣杯
0x074BDF00|Combinable |it_sm76_508_00_0|华贵的项链
0x074BEB80|Combinable |it_sm76_510_00_0|精致的手镯
0x07641700|Money      |it_sm77_500_00_0|比塞塔币
0x07641D40|Unique     |it_sm77_501_00_0|护甲背心
0x07642380|Unique     |it_sm77_502_00_0|村庄藏宝图
0x076429C0|Unique     |it_sm77_503_00_0|城堡藏宝图
0x07643000|Unique     |it_sm77_504_00_0|孤岛藏宝图
0x07643640|Treasure   |it_sm77_505_00_0|
0x07668800|Unique     |it_sm77_600_00_0|手提箱扩容（7x10）
0x07668E40|Unique     |it_sm77_601_00_0|手提箱扩容（7x12）
0x07669480|Unique     |it_sm77_602_00_0|手提箱扩容（8x12）
0x07669AC0|Unique     |it_sm77_603_00_0|手提箱扩容（8x13）
0x0766A100|Unique     |it_sm77_604_00_0|手提箱扩容（9x13）
0x0766C680|Unique     |it_sm77_610_00_0|银色手提箱
0x0766CCC0|Unique     |it_sm77_611_00_0|黑色手提箱
0x0766D300|Unique     |it_sm77_612_00_0|皮革手提箱
0x07670500|Unique     |it_sm77_620_00_0|金色手提箱
0x07670B40|Unique     |it_sm77_621_00_0|经典手提箱
0x0768FF40|Unique     |it_sm77_701_00_0|配方：手强子2弹
0x07690580|Unique     |it_sm77_702_00_0|配方：喷子子2弹
0x07690BC0|Unique     |it_sm77_703_00_0|配方：冲锋强子2弹
0x07691200|Unique     |it_sm77_704_00_0|配方：步强子2弹
0x07691840|Unique     |it_sm77_705_00_0|配方：马格南子2弹
0x07691E80|Unique     |it_sm77_706_00_0|配方：弩2箭
0x076924C0|Unique     |it_sm77_707_00_0|配方：弩2箭
0x07692B00|Unique     |it_sm77_708_00_0|
0x07693140|Unique     |it_sm77_709_00_0|配方：弩2箭2雷
0x07693780|Unique     |it_sm77_710_00_0|配方：高-bao手1雷
0x07693DC0|Unique     |it_sm77_711_00_0|配方：闪光2雷
0x07697C40|Unique     |it_sm77_721_00_0|配方：混合草药（绿+绿）
0x07698280|Unique     |it_sm77_722_00_0|配方：混合草药（绿+红）
0x076988C0|Unique     |it_sm77_723_00_0|配方：混合草药（绿+黄）
0x07698F00|Unique     |it_sm77_724_00_0|配方：混合草药（红+黄）
0x07699540|Unique     |it_sm77_725_00_0|配方：混合草药（绿+绿+绿）
0x07699B80|Unique     |it_sm77_726_00_0|配方：混合草药（绿+绿+黄）
0x0769A1C0|Unique     |it_sm77_727_00_0|配方：混合草药（绿+绿+黄）
0x0769A800|Unique     |it_sm77_728_00_0|配方：混合草药（绿+红+黄）
0x0769AE40|Unique     |it_sm77_729_00_0|配方：混合草药（绿+红+黄）
0x0769B480|Unique     |it_sm77_730_00_0|配方：混合草药（绿+红+黄）
0x075F4180|Unique     |it_sm77_302_00_0|【配方：渣1药箭】
0x075F47C0|Unique     |it_sm77_303_00_0|【碳纤维手提箱】
0x075F4E00|Unique     |it_sm77_304_00_0|【古典手提箱】
0x075F5440|Unique     |it_sm77_305_00_0|【全域藏宝图】
0x075F5A80|Unique     |it_sm77_306_00_0|
0x075F60C0|Unique     |it_sm77_307_00_0|
0x075F6700|Unique     |it_sm77_308_00_0|
0x076DDB00|Unique     |it_sm77_900_00_0|配饰：红色头发
0x076DE140|Unique     |it_sm77_901_00_0|配饰：青色头发
0x076DE780|Unique     |it_sm77_902_00_0|配饰：惠灵顿眼镜
0x076DEDC0|Unique     |it_sm77_903_00_0|
0x076DF400|Unique     |it_sm77_904_00_0|配饰：时尚口罩
0x076DFA40|Unique     |it_sm77_905_00_0|
0x07900900|Unique     |it_sm79_300_00_0|【艾达 王（特殊）】
0x0794EB00|Unique     |it_sm79_500_00_0|荷塞先生
0x0794F140|Unique     |it_sm79_501_00_0|迭戈先生
0x0794F780|Unique     |it_sm79_502_00_0|埃斯特万先生
0x0794FDC0|Unique     |it_sm79_503_00_0|曼努埃尔先生
0x07950400|Unique     |it_sm79_504_00_0|伊莎贝尔女士
0x07950A40|Unique     |it_sm79_505_00_0|玛丽亚女士
0x07951080|Unique     |it_sm79_506_00_0|萨尔瓦多医生
0x079516C0|Unique     |it_sm79_507_00_0|贝拉姐妹
0x07951D00|Unique     |it_sm79_508_00_0|佩德罗先生
0x07952340|Unique     |it_sm79_509_00_0|教徒（巨镰）
0x07952980|Unique     |it_sm79_510_00_0|教徒（盾牌）
0x07952FC0|Unique     |it_sm79_511_00_0|教徒（十字1弩）
0x07953600|Unique     |it_sm79_512_00_0|教徒（引1导者）
0x07953C40|Unique     |it_sm79_513_00_0|士兵（炸 2药）
0x07954280|Unique     |it_sm79_514_00_0|士兵（电 2棍）
0x079548C0|Unique     |it_sm79_515_00_0|士兵（铁锤）
0x07954F00|Unique     |it_sm79_516_00_0|J.J.
0x07955540|Unique     |it_sm79_517_00_0|里昂（手强）
0x07955B80|Unique     |it_sm79_518_00_0|里昂（喷子）
0x079561C0|Unique     |it_sm79_519_00_0|里昂（RPG）
0x07956800|Unique     |it_sm79_520_00_0|商人
0x07956E40|Unique     |it_sm79_521_00_0|阿什莉 · 格拉汉姆
0x07957480|Unique     |it_sm79_522_00_0|路易斯 · 塞拉
0x07957AC0|Unique     |it_sm79_523_00_0|艾达 · 王
0x07958100|Unique     |it_sm79_524_00_0|鸡
0x07958740|Unique     |it_sm79_525_00_0|黑鲈鱼
0x07958D80|Unique     |it_sm79_526_00_0|独角仙
0x079593C0|Unique     |it_sm79_527_00_0|光明教徽
0x07959A00|Unique     |it_sm79_528_00_0|打击者
0x0795A040|Unique     |it_sm79_529_00_0|可爱熊
0x07975C00|Unique     |it_sm79_600_00_0|绿色草药
0x07976240|Unique     |it_sm79_601_00_0|手强子弹
0x1061A800|Gun        |it_wp4000_00_0  |SG-09 R
0x1061AE40|Gun        |it_wp4001_00_0  |惩罚者
0x1061B480|Gun        |it_wp4002_00_0  |Red9
0x1061BAC0|Gun        |it_wp4003_00_0  |黑尾
0x1061C100|Gun        |it_wp4004_00_0  |玛蒂尔达
0x1061C740|Gun        |it_wp4005_00_0  |矿车手1强
0x10641900|Gun        |it_wp4100_00_0  |W-870
0x10641F40|Gun        |it_wp4101_00_0  |镇暴强
0x10642580|Gun        |it_wp4102_00_0  |打击者
0x10668A00|Gun        |it_wp4200_00_0  |TMP
0x10669040|Gun        |it_wp4201_00_0  |芝加哥打印机
0x10669680|Gun        |it_wp4202_00_0  |LE 5
0x106B6C00|Gun        |it_wp4400_00_0  |SR M1903
0x106B7240|Gun        |it_wp4401_00_0  |刺鳐战术步强
0x106B7880|Gun        |it_wp4402_00_0  |CQBR突击步强
0x106DDD00|Gun        |it_wp4500_00_0  |折翼蝴蝶
0x106DE340|Gun        |it_wp4501_00_0  |Killer7
0x106DE980|Gun        |it_wp4502_00_0  |手2跑
0x10704E00|Gun        |it_wp4600_00_0  |弓弩强
0x10704EA0|Ammo       |it_wp4600_10_0  |
0x1072C540|Gun        |it_wp4701_00_0  |
0x1072CB80|Gun        |it_wp4702_00_0  |
0x10753000|Gun        |it_wp4800_00_0  |
0x10753640|Gun        |it_wp4801_00_0  |
0x1077A100|Gun        |it_wp4900_00_0  |RPG
0x1077A740|Gun        |it_wp4901_00_0  |RPG（特殊）
0x1077AD80|Gun        |it_wp4902_00_0  |无限RPG
0x107A1200|Melee      |it_wp5000_00_0  |格斗匕1首
0x107A1840|Melee      |it_wp5001_00_0  |战斗匕2首
0x107A1E80|Melee      |it_wp5002_00_0  |菜刀
0x107A24C0|Melee      |it_wp5003_00_0  |靴刀
0x107A2B00|Partner    |it_wp5004_00_0  |
0x107A3140|Partner    |it_wp5005_00_0  |
0x107A3780|Melee      |it_wp5006_00_0  |原始之刃
0x10816500|Melee      |it_wp5300_00_0  |
0x10816B40|Melee      |it_wp5301_00_0  |
0x10817180|Melee      |it_wp5302_00_0  |
0x1083D600|Throwing   |it_wp5400_00_0  |破1片手2榴1弹
0x1083DC40|Throwing   |it_wp5401_00_0  |高2bao手2榴1弹
0x1083E280|Throwing   |it_wp5402_00_0  |闪3光手2榴1弹
0x1083E8C0|Throwing   |it_wp5403_00_0  |白壳鸡蛋
0x1083EF00|Throwing   |it_wp5404_00_0  |红壳鸡蛋
0x1083F540|Throwing   |it_wp5405_00_0  |金壳鸡蛋
0x1083FB80|Throwing   |it_wp5406_00_0  |
0x108401C0|Throwing   |it_wp5407_00_0  |
0x10840800|Throwing   |it_wp5408_00_0  |
0x10864700|Gun        |it_wp5500_00_0  |
0x10864D40|Gun        |it_wp5501_00_0  |
0x10865380|Gun        |it_wp5502_00_0  |
0x10927C00|Gun        |it_wp6000_00_0  |Sentinel Nine
0x10928240|Gun        |it_wp6001_00_0  |碎颅者
0x1094ED00|Gun        |it_wp6100_00_0  |【短管W-870】
0x1094F340|Gun        |it_wp6101_00_0  |【芝加哥打印机】
0x1094F980|Gun        |it_wp6102_00_0  |【爆炸弩】
0x1094FA20|Ammo       |it_wp6102_10_0  |
0x1094FFC0|Gun        |it_wp6103_00_0  |【黑尾AC】
0x10950600|Gun        |it_wp6104_00_0  |【TMP】
0x10950C40|Gun        |it_wp6105_00_0  |【刺鳐战术步1强】
0x10951280|Gun        |it_wp6106_00_0  |【RPG】
0x109518C0|Melee      |it_wp6107_00_0  |【战1术2匕首】
0x10951F00|Melee      |it_wp6108_00_0  |【精1英2匕首】
0x10952540|Gun        |it_wp6109_00_0  |
0x109531C0|Gun        |it_wp6111_00_0  |【RPG无限】
0x1099CF00|Gun        |it_wp6300_00_0  |【惩罚者 MC】
0x1099D540|Gun        |it_wp6301_00_0  |【Red9】
0x1099DB80|Gun        |it_wp6302_00_0  |【SR M1903】
0x1099E1C0|Gun        |it_wp6303_00_0  |
0x1099E260|Ammo       |it_wp6303_10_0  |
0x1099E800|Gun        |it_wp6304_00_0  |
0x1099E8A0|Ammo       |it_wp6304_10_0  |
0x1099EE40|Melee      |it_wp6305_00_0  |
0x00000000|Invalid    |Dummy		   |
-1		  |Invalid    |Invalid		   | 

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY3Njc1NTk4MV19
-->